<?php
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="sideBars <?php print $block->region; ?> block">
<?php if (!empty($block->subject)): ?>
  <h3><?php print $block->subject ?></h3>
<?php endif;?>
<div class="content"><?php print $block->content ?></div>
</div>
<div class="<?php print $block->region;?>-end"> </div>
