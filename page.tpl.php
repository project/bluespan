<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body>
  <!-- begin container -->
  <div id="container">
    <div class="header">
      <?php
      if ($logo || $site_name) {
        print '<div class="logo">';
        if ($logo) {
          print '<img src="'. check_url($logo) .'" alt="'. $site_name .'" id="logo" />';
        }
        print '<h1><a href="'. check_url($front_page) .'" title="'. $site_name .'">' . $site_name . '</a>';
        print '<br class="clear" />';
        print '</div>';
      }
      ?>
    </div>

    <!-- primary links -->
    <?php if (isset($primary_links)) : ?>
    <?php print '<div id="menu">'; ?>
    <?php print theme('links', $primary_links, array('class' => 'primary-links')) ?>
    <?php print '</div>'; ?>
    <?php endif; ?>

    <!-- end primary links -->


    <!-- content -->

    <!-- begin mainContent -->
   <div class="main">
    <div id="mainContent">
      <div class="mainContent-content">
        <?php if ($title) { ?><h1><?php print $title ?></h1><?php } ?>
        <?php if ($tabs): print '<div><ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
        <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
        <?php if ($show_messages && $messages): print $messages; endif; ?>
        <br />
        <?php print $content; ?>
      </div>
      <div class="mainContent-end"> </div>
      <!-- footer -->
      <div id="footer">
       <?php if ($footer) { print $footer; } ?>
       <?php if ($footer_message) { print $footer_message; } ?>
      </div><!-- end footer -->
    </div>
    <!-- end mainContent -->

    <!-- begin sideBars -->

    <div id="sideBars-bg">
    <?php if ($mission) { ?>
      <div id="mission" class="sideBars">
        <?php print $mission; ?>
      </div>
      <div class="blue-end"> </div>
    <?php } ?>

    <?php if ($blue) { echo $blue; } ?>
    <?php if ($white) { echo $white; } ?>
    </div><!-- end sideBars-bg -->
    
    <br class="clear" />
   </div>
    
  </div><!-- end container -->
</body>
</html>
