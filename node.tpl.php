<?php
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php print $picture ?>

<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

 <div class="meta">
  <?php if ($submitted): ?>
    <span class="submitted"><?php print $submitted; ?></span>
  <?php endif; ?>
    <?php if ($taxonomy): ?>
    <?php if ($submitted): ?><span class="meta-sep"> | </span><?php endif; ?>
    <span class="terms"><?php print $terms ?></span>
    <?php endif;?>
 </div>

  <div class="content clear-block">
    <?php print $content ?>
  </div>

  <div class="clear-block">
    <?php if ($links): ?>
      <div class="links"><?php print $links; ?></div>
    <?php endif; ?>
  </div>

  <div class="node-sep"><hr />&nbsp;</div>
</div> <!-- node end -->
